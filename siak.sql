-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2018 at 08:13 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siak`
--

-- --------------------------------------------------------

--
-- Table structure for table `ekskul`
--

CREATE TABLE `ekskul` (
  `id` varchar(30) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ekskul`
--

INSERT INTO `ekskul` (`id`, `nama`) VALUES
('1ZrAXIEHs6Fg7iJ', 'KIR'),
('6s0M7vh3BFzcwR8', 'Karate'),
('BVqGvakOLJ8dprx', 'Palasfi'),
('ExnHYFT0jJvdcD7', 'Rohis Putra'),
('Lyi1CuZA5nJUWYp', 'Tahfidz'),
('ODU7IBQAPNafwi2', 'PMR'),
('Qd9FebtVB3PfaN7', 'Rohis Putri'),
('s6UCSPi8qocIE2g', 'Paskibra'),
('sc8ibUnvHBkw1de', 'Drumband'),
('x9bW8Ifqzae2yZY', 'Pramuka');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id` varchar(15) NOT NULL,
  `NIP` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ttl` varchar(225) NOT NULL,
  `jabatan` varchar(120) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `agama` varchar(20) NOT NULL,
  `warganegara` varchar(20) NOT NULL,
  `nope` varchar(15) NOT NULL,
  `alamat` varchar(2000) NOT NULL,
  `photo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `NIP`, `nama`, `ttl`, `jabatan`, `jk`, `agama`, `warganegara`, `nope`, `alamat`, `photo`) VALUES
('FBf5UYHRul40Se6', '331015', 'HENDRI', 'KLATEN 26-05-1998', 'WALI KELAS', 'L', '', '', '081226085836', 'Griya Sanggrahan Indah 3 C-11 Rt 06 Rw 22, Makamhaji, Kartasura, Sukoharjo', 'hendri.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_pelajaran`
--

CREATE TABLE `jadwal_pelajaran` (
  `id` varchar(15) NOT NULL,
  `id_kelas` varchar(20) NOT NULL,
  `kode_mapel` varchar(20) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `jam_awal` int(2) NOT NULL,
  `menit_awal` int(2) NOT NULL,
  `jam_akhir` int(2) NOT NULL,
  `menit_akhir` int(2) NOT NULL,
  `nip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kuesioner`
--

CREATE TABLE `kuesioner` (
  `id_kuesioner` int(11) NOT NULL,
  `pertanyaan` text NOT NULL,
  `metode` varchar(20) NOT NULL,
  `jenis` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kuesioner`
--

INSERT INTO `kuesioner` (`id_kuesioner`, `pertanyaan`, `metode`, `jenis`) VALUES
(1, 'Berdoa sebelum dan sesudah melakukan sesuat', 'observasi', '	spiritual'),
(2, 'Menjalankan  ibadah tepat waktu', 'observasi', '	spiritual'),
(3, 'Mengucapkan rasa syukur atas karuna Tuhan', 'observasi', '	spiritual'),
(4, 'Memberi salam sebelum dan sesudah menyampaikan pendapat/presentasi', 'observasi', '	spiritual'),
(5, 'Mungucapkan syukur ketika berhasil mengerjakan sesuatu', 'observasi', 'spiritual'),
(6, 'Berserah diri (tawakal) kepada Allah setelah berikhtiar atau melakukan usaha', 'observasi', '	spiritual'),
(7, 'Mengungkapkan kekaguman secara lisan maupun tulisan terhadap Allah saat melihat kebesaran Allah', 'observasi', '	spiritual'),
(8, 'Menambah rasa keimanan akan keberadaan dan kebesaran Tuhan saat mempelajari ilmu pengetahuan', 'observasi', '	spiritual'),
(9, 'Tidak nyontek dalam mengerjakan ujian/ulangan', 'observasi', '	jujur'),
(10, 'Tidak melakukan plagiat (mengambil/menyalin karya orang lain tanpa menyebutkan sumber) dalam mengerjakan setiap tugas', 'observasi', '	jujur'),
(11, 'Mengemukakan perasaan terhadap sesuatu apa adanya', 'observasi', '	jujur'),
(12, 'Melaporkan data atau informasi apa adanya	', 'observasi', '	jujur'),
(13, 'Mengakui kesalahan atau kekurangan yang dimiliki	', 'observasi', '	jujur'),
(14, 'Masuk kelas tepat waktu	', 'observasi', '	disiplin'),
(15, 'Mengumpulkan tugas tepat waktu	', 'observasi', '	disiplin'),
(16, 'Memakai seragam sesuai tata tertib	', 'observasi', '	disiplin'),
(17, 'Mengerjakan tugas yang diberikan	', 'observasi', '	disiplin'),
(18, 'Tertib dalam mengikuti pembelajaran	', 'observasi', '	disiplin'),
(19, 'Mengikuti praktikum sesuai dengan langkah yang ditetapkan	', 'observasi', '	disiplin'),
(20, 'Membawa buku tulis sesuai mata pelajaran	', 'observasi', '	disiplin'),
(21, 'Membawa buku teks mata pelajaran	', 'observasi', '	disiplin'),
(22, 'Melaksanakan tugas individu dengan baik	', 'observasi', '	tanggungjawab'),
(23, 'Menerima resiko dari tindakan yang dilakukan	', 'observasi', '	tanggungjawab'),
(24, 'Tidak menuduh orang lain tanpa bukti yang akurat	', 'observasi', '	tanggungjawab'),
(25, 'Mengembalikan barang yang dipinjam	', 'observasi', '	tanggungjawab'),
(26, 'Meminta maaf atas kesalahan yang dilakukan	', 'observasi', '	tanggungjawab'),
(27, 'Menghormati pendapat teman	', 'observasi', '	toleransi'),
(28, 'Menghormati teman yang berbeda suku, agama, ras, budaya, dan gender	', 'observasi', '	toleransi'),
(29, 'Menerima kesepakatan meskipun berbeda dengan pendapatnya	', 'observasi', '	toleransi'),
(30, 'Menerima kekurangan orang lain	', 'observasi', '	toleransi'),
(31, 'Mememaafkan kesalahan orang lain	', 'observasi', '	toleransi'),
(32, 'Aktif dalam kerja kelompok	', 'observasi', '	gotong royong'),
(33, 'Suka menolong teman/orang lain	', 'observasi', '	gotong royong'),
(34, 'Kesediaan melakukan tugas sesuai kesepakatan	', 'observasi', '	gotong royong'),
(35, 'Rela berkorban untuk orang lain	', 'observasi', '	gotong royong'),
(36, 'Menghormati orang yang lebih tua	', 'observasi', '	santun'),
(37, 'Mengucapkan terima kasih setelah menerima bantuan orang lain	', 'observasi', '	santun'),
(38, 'Menggunakan bahasa santun saat menyampaikan pendapat	', 'observasi', '	santun'),
(39, 'Menggunakan bahasa santun saat mengkritik pendapat teman	', 'observasi', '	santun'),
(40, 'Bersikap 3S (salam, senyum, sapa) saat bertemu orang lain	', 'observasi', '	santun'),
(41, 'Berani presentasi di depan kelas	', 'observasi', '	percaya diri'),
(42, 'Berani berpendapat, bertanya atau menjawab pertanyaan	', 'observasi', '	percaya diri'),
(43, 'Berpendapat atau melakukan kegiatan tanpa ragu-ragu	', 'observasi', '	percaya diri'),
(44, 'Mampu membuat keputusan dengan cepat	', 'observasi', '	percaya diri'),
(45, 'Tidak mudah putus asa/pantang menyerah	', 'observasi', '	percaya diri');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id` varchar(15) NOT NULL,
  `kode_mapel` varchar(25) NOT NULL,
  `nama_mapel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id`, `kode_mapel`, `nama_mapel`) VALUES
('3CnKvQiDqNMgmf4', 'L  ', 'Seni Budaya    '),
('aukNAYWB4soPMRZ', 'A    ', 'Al-qur\'an Hadits  '),
('bXGhUT1J7WOnipL', 'P   ', 'Prakarya   '),
('CMcFp7qSrNz3aHW', 'E  ', 'Pendidikan Kewarganegaraan    '),
('DzTmncX87VGwZ5u', 'D  ', 'Sejarah Kebudayaan Islam    '),
('EmoayftWxQrvkVT', 'J   ', 'Ilmu Pengetahuan Alam   '),
('JiKZ9UHasycmF2x', 'C    ', 'Fiqih  '),
('kiEeNghvIdpM6AF', 'B  ', 'Aqidah Akhlak    '),
('kMdS6QfYmncEHqR', 'H   ', 'Bahasa Inggris   '),
('lITpn0FZs3m5VCc', 'I  ', 'Matematika    '),
('r0SlOqLxWAKVBZw', 'K    ', 'Ilmu Pengetahuan Sosial   '),
('RsMpqWJIZtYgjno', 'G  ', 'Bahasa Arab    '),
('U92XGx7s3VjmThH', 'N    ', 'TIK  '),
('ULAozCt29FQx3q8', 'M   ', 'PJOK   '),
('Z3CxvRNtyJqWTrI', 'F  ', 'Bahasa Indonesia    ');

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `id_materi` int(15) NOT NULL,
  `id_mapel` varchar(15) NOT NULL,
  `id_kelas` varchar(20) NOT NULL,
  `materi` varchar(100) NOT NULL,
  `flag` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id_materi`, `id_mapel`, `id_kelas`, `materi`, `flag`) VALUES
(5, 'EmoayftWxQrvkVT', 'IXh8kDqGdp', 'Kontrak Pelajaran', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_ekskul`
--

CREATE TABLE `nilai_ekskul` (
  `id` int(5) NOT NULL,
  `nik_siswa` varchar(15) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nilai` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nilai_siswa`
--

CREATE TABLE `nilai_siswa` (
  `id` varchar(15) NOT NULL,
  `nik_siswa` varchar(20) NOT NULL,
  `id_kelas` varchar(25) NOT NULL,
  `kode_mapel` varchar(20) NOT NULL,
  `semester` enum('Ganjil','Genap') NOT NULL,
  `thn_ajaran` varchar(10) NOT NULL,
  `tugas` decimal(10,0) NOT NULL,
  `uts` decimal(10,0) NOT NULL,
  `uas` decimal(10,0) NOT NULL,
  `rata` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_siswa`
--

INSERT INTO `nilai_siswa` (`id`, `nik_siswa`, `id_kelas`, `kode_mapel`, `semester`, `thn_ajaran`, `tugas`, `uts`, `uas`, `rata`) VALUES
('P0KA1vVkOCX7xjf', '3310152605980001', 'IXh8kDqGdp', 'A    ', 'Ganjil', '2018-2019', '90', '95', '100', '95');

-- --------------------------------------------------------

--
-- Table structure for table `ruang_kelas`
--

CREATE TABLE `ruang_kelas` (
  `id` varchar(20) NOT NULL,
  `nama_ruangan` varchar(6) NOT NULL,
  `jumlah_siswa` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang_kelas`
--

INSERT INTO `ruang_kelas` (`id`, `nama_ruangan`, `jumlah_siswa`) VALUES
('1ezDOKixgY', 'IX C', 25),
('1px8rY6aGW', 'VIII E', 25),
('7VHscgPdKn', 'VII E', 25),
('BrQqSnOGVR', 'VIII D', 25),
('CJHKtzyFpD', 'VII F', 25),
('diTOGIQhen', 'VIII C', 25),
('dtp4BwUITW', 'VIII B', 25),
('FbsPWLmxCv', 'VII D', 25),
('Go1YELwsnr', 'IX F', 25),
('IXh8kDqGdp', 'VII A', 25),
('lO4uhHJiDQ', 'IX D', 25),
('NfEWTOcI8S', 'IX B', 25),
('NP0MBpEOoc', 'IX E', 25),
('Okfv0YNmp1', 'VIII A', 25),
('pEb03aPeIQ', 'VIII F', 25),
('rkw6Sbqcpv', 'IX A', 25),
('xQ0Z1q9PVa', 'VII B', 25),
('zxhRJjQ5T8', 'VII C', 25);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` varchar(15) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `id_kelas` varchar(20) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `nama` varchar(50) NOT NULL,
  `thn_ajaran` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `ttl` varchar(225) NOT NULL,
  `nope` varchar(13) NOT NULL,
  `photo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `nik`, `id_kelas`, `jk`, `nama`, `thn_ajaran`, `alamat`, `ttl`, `nope`, `photo`) VALUES
('AqDFEBwLMsOkmt1', '3310152605980001', 'OkKvx4C2Fn', 'L', 'HAIDAR HENDRI SETYAWAN', '2018-2019', 'Griya Sanggrahan Indah 3 C-11 Rt 06 Rw 22, Makamhaji, Kartasura, Sukoharjo', 'KLATEN 26-05-1998', '081226085836', 'pp1.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_has_kelas`
--

CREATE TABLE `siswa_has_kelas` (
  `id` varchar(15) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `id_kelas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa_has_kelas`
--

INSERT INTO `siswa_has_kelas` (`id`, `nik`, `id_kelas`) VALUES
('AqDFEBwLMsOkmt1', '3310152605980001', 'OkKvx4C2Fn');

-- --------------------------------------------------------

--
-- Table structure for table `siswa_has_kuesioner`
--

CREATE TABLE `siswa_has_kuesioner` (
  `id_jawaban` int(11) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `id_kuesioner` int(11) NOT NULL,
  `tidak_pernah` int(11) NOT NULL,
  `kadang_kadang` int(11) NOT NULL,
  `sering` int(11) NOT NULL,
  `selalu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE `surat` (
  `id_surat` int(11) NOT NULL,
  `nomor_surat` varchar(20) NOT NULL,
  `perihal` varchar(50) NOT NULL,
  `tujuan_surat` varchar(10) NOT NULL,
  `tanggal_surat` varchar(10) NOT NULL,
  `pengolah` varchar(10) NOT NULL,
  `tanggal_terima` varchar(60) NOT NULL,
  `keterangan` text NOT NULL,
  `upload` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat`
--

INSERT INTO `surat` (`id_surat`, `nomor_surat`, `perihal`, `tujuan_surat`, `tanggal_surat`, `pengolah`, `tanggal_terima`, `keterangan`, `upload`) VALUES
(6, '02/UNS/IF/2016', 'UNDANGAN', 'guru', '07-12-2018', 'HENDRI', 'Sat Dec 08 2018 17:02:57 GMT+0700 (Western Indonesia Time)', 'SEGERA YAA', 'Data_Belum_Seminar_KMM1.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(15) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nik`, `nama`, `username`, `password`, `level`) VALUES
('0GAmF2S4ZDLXYp9', '0', 'admin', 'admin', 'admin', 'admin'),
('AqDFEBwLMsOkmt1', '3310152605980001', 'HAIDAR HENDRI SETYAWAN', 'siswa', 'siswa', 'siswa'),
('FBf5UYHRul40Se6', '331015', 'HENDRI', 'guru', 'guru', 'guru');

-- --------------------------------------------------------

--
-- Table structure for table `wali_kelas`
--

CREATE TABLE `wali_kelas` (
  `id` varchar(15) NOT NULL,
  `NIP` varchar(25) NOT NULL,
  `id_kelas` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wali_kelas`
--

INSERT INTO `wali_kelas` (`id`, `NIP`, `id_kelas`) VALUES
('MuAWwVIPTvZUcoS', '331015', 'IXh8kDqGdp');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ekskul`
--
ALTER TABLE `ekskul`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `NIP` (`NIP`);

--
-- Indexes for table `jadwal_pelajaran`
--
ALTER TABLE `jadwal_pelajaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jadwal_kode_mapel_mapel` (`kode_mapel`),
  ADD KEY `jadwal_kelas_kelas` (`id_kelas`),
  ADD KEY `jadwal_nip_guru` (`nip`);

--
-- Indexes for table `kuesioner`
--
ALTER TABLE `kuesioner`
  ADD PRIMARY KEY (`id_kuesioner`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_mapel` (`kode_mapel`);

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id_materi`),
  ADD KEY `id_mapel` (`id_mapel`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `nilai_ekskul`
--
ALTER TABLE `nilai_ekskul`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_siswa`
--
ALTER TABLE `nilai_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nik_siswa` (`nik_siswa`),
  ADD KEY `kode_mapel` (`kode_mapel`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `ruang_kelas`
--
ALTER TABLE `ruang_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nik` (`nik`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indexes for table `siswa_has_kelas`
--
ALTER TABLE `siswa_has_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `nik` (`nik`);

--
-- Indexes for table `siswa_has_kuesioner`
--
ALTER TABLE `siswa_has_kuesioner`
  ADD PRIMARY KEY (`id_jawaban`),
  ADD KEY `nik` (`nik`,`id_kuesioner`),
  ADD KEY `id_kuesioner` (`id_kuesioner`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`id_surat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wali_kelas`
--
ALTER TABLE `wali_kelas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kuesioner`
--
ALTER TABLE `kuesioner`
  MODIFY `id_kuesioner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `materi`
--
ALTER TABLE `materi`
  MODIFY `id_materi` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `nilai_ekskul`
--
ALTER TABLE `nilai_ekskul`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `siswa_has_kuesioner`
--
ALTER TABLE `siswa_has_kuesioner`
  MODIFY `id_jawaban` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `id_surat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwal_pelajaran`
--
ALTER TABLE `jadwal_pelajaran`
  ADD CONSTRAINT `jadwal_kelas_kelas` FOREIGN KEY (`id_kelas`) REFERENCES `ruang_kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jadwal_kode_mapel_mapel` FOREIGN KEY (`kode_mapel`) REFERENCES `mapel` (`kode_mapel`) ON DELETE CASCADE,
  ADD CONSTRAINT `jadwal_nip_guru` FOREIGN KEY (`nip`) REFERENCES `guru` (`NIP`) ON DELETE CASCADE;

--
-- Constraints for table `materi`
--
ALTER TABLE `materi`
  ADD CONSTRAINT `materi_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `ruang_kelas` (`id`),
  ADD CONSTRAINT `materi_ibfk_2` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id`);

--
-- Constraints for table `nilai_siswa`
--
ALTER TABLE `nilai_siswa`
  ADD CONSTRAINT `nilai_siswa_ibfk_1` FOREIGN KEY (`nik_siswa`) REFERENCES `siswa` (`nik`),
  ADD CONSTRAINT `nilai_siswa_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `ruang_kelas` (`id`);

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`id_kelas`) REFERENCES `ruang_kelas` (`id`);

--
-- Constraints for table `siswa_has_kelas`
--
ALTER TABLE `siswa_has_kelas`
  ADD CONSTRAINT `id_kelas` FOREIGN KEY (`id_kelas`) REFERENCES `ruang_kelas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `nik` FOREIGN KEY (`nik`) REFERENCES `siswa` (`nik`) ON DELETE CASCADE;

--
-- Constraints for table `siswa_has_kuesioner`
--
ALTER TABLE `siswa_has_kuesioner`
  ADD CONSTRAINT `siswa_has_kuesioner_ibfk_1` FOREIGN KEY (`id_kuesioner`) REFERENCES `kuesioner` (`id_kuesioner`),
  ADD CONSTRAINT `siswa_has_kuesioner_ibfk_2` FOREIGN KEY (`nik`) REFERENCES `siswa` (`nik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
