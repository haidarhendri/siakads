<?php
defined('BASEPATH') OR exit('No direct scripts access allowed');

class Nilai extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$logged_in = $this->session->userdata('logged_in');
		$level = $this->session->userdata('level');
		if(empty($logged_in))
		{
			redirect('auth/login');
		}
		if($level != 'siswa')
		{
			redirect('auth/login');
		}
	}

	public function index()
	{
		$nik = $this->session->userdata('nik');
		$data['profil'] = $this->db->where('nik',$nik)->get('siswa')->row();
	}

	public function semesterganjil()
	{
		$nik_siswa = $this->session->userdata('nik');
		$data['profil'] = $this->db->where('nik',$nik_siswa)->get('siswa')->row();
		$data['nilai'] = $this->db->select('nilai_siswa.*,mapel.nama_mapel')->from('nilai_siswa')->join('mapel','mapel.kode_mapel=nilai_siswa.kode_mapel')->where('semester','Ganjil')->where('nik_siswa',$nik_siswa)->get()->result();
		$this->load->view('user/nilai/ganjil/index',$data);
	}

	public function cetak()
	{
		$nik_siswa = $this->session->userdata('nik');
		$data['nilai'] = $this->db->select('nilai_siswa.*,mapel.nama_mapel')->from('nilai_siswa')->join('mapel','mapel.kode_mapel=nilai_siswa.kode_mapel')->where('semester','Ganjil')->where('nik_siswa',$nik_siswa)->get()->result();
		$html = $this->load->view('user/nilai/ganjil/pdf',$data,true);

		require(APPPATH."/third_party/html2pdf_4_03/html2pdf.class.php");
		try {
			$html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array('20', '5', '20', '5'));
			$html2pdf->WriteHTML($html);
			$html2pdf->Output('laporan_nilai_'.date('Ymd').'.pdf');
		} catch (HTML2PDF_exception $e) {
            echo $e;
		}
	}
}
