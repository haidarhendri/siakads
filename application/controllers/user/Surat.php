<?php
defined('BASEPATH') OR exit('No direct scripts access allowed');

class Surat extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$logged_in = $this->session->userdata('logged_in');
		$level = $this->session->userdata('level');
		if(empty($logged_in))
		{
			redirect('home');
		}
		if($level != 'siswa')
		{
			redirect('home');
		}

	}

	public function index()
	{
		$nik = $this->session->userdata('nik');
		$data['profil'] = $this->db->where('nik',$nik)->get('siswa')->row();
    $data['surat'] = $this->db->get('surat')->result();
    $this->load->view('user/surat/index',$data);
    }

    public function surat()
  {
		$nik = $this->session->userdata('nik');
		$data['profil'] = $this->db->where('nik',$nik)->get('siswa')->row();
    $data['surat'] = $this->db->get('surat')->result();
    $this->load->view('user/surat/index',$data);
  }


  public function cetaksurat()
  {
    $data['surat'] = $this->db->get('surat')->result();
    $html = $this->load->view('user/surat/cetak',$data, true);

    require(APPPATH."/third_party/html2pdf_4_03/html2pdf.class.php");
    try {
      $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array('20', '5', '20', '5'));
      $html2pdf->WriteHTML($html);
      $html2pdf->Output('laporan_surat_'.date('Ymd').'.pdf');
    } catch (HTML2PDF_exception $e) {
      $this->session->set_flashdata('berhasil', 'Maaf, kami mengalami kendala teknis.');
      redirect('user/surat');
    }
  }
}
