<?php
defined('BASEPATH') OR exit('No direct scripts access allowed');

class Surat extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$logged_in = $this->session->userdata('logged_in');
		$level = $this->session->userdata('level');
		if(empty($logged_in))
		{
			redirect('auth/login');
		}
		if($level != 'admin')
		{
			redirect('auth/login');
		}
	}

	//Memanggil form untuk tambah surat
	public function create()
	{
		$this->load->view('admin/surat/create');
	}

	//Menyimpan data baru surat ke database
	public function store()
	{
		$config['upload_path']          = './assets/upload/surat';
		$config['allowed_types']        = 'gif|jpg|png|pdf|docx|doc';
		$config['max_size']             = 2000;
		$config['max_width']            = 2000;
		$config['max_height']           = 2000;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$data['error'] = $this->upload->display_errors();
			$this->load->view('admin/surat/create',$data);
		}
		else
		{
			$namaphoto = $this->upload->data('file_name');

			do{
			$id = random_string('alnum',15);
			$cek = $this->db->where('id_surat',$id)->get('surat')->result();
			$cek_id = count($cek);
		}while($cek_id > 0);

			$data['id_surat'] = $id;
			$data['nomor_surat'] = $this->input->post('nomor_surat',true);
			$data['perihal'] = $this->input->post('perihal',true);
			$data['tujuan_surat'] = $this->input->post('tujuan_surat',true);
			$data['tanggal_surat'] = $this->input->post('tanggal_surat',true);
			$data['pengolah'] = $this->input->post('pengolah',true);
			$data['tanggal_terima'] = $this->input->post('tanggal_terima',true);
			$data['keterangan'] = $this->input->post('keterangan',true);
			$data['upload'] = $namaphoto;

			$this->db->insert('surat',$data);

			$this->session->set_flashdata('berhasil','Surat berhasil ditambahkan');
			redirect('admin/surat/create');
		}
	}
}
