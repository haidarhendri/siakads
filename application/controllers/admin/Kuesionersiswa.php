<?php
defined('BASEPATH') OR exit('No direct scripts access allowed');

class Kuesionersiswa extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$logged_in = $this->session->userdata('logged_in');
		$level = $this->session->userdata('level');
		if(empty($logged_in))
		{
			redirect('auth/login');
		}
		if($level != 'admin')
		{
			redirect('auth/login');
		}
	}

	public function index()
	{
		$data['nik'] = $this->session->userdata('nik');
		$this->load->view('admin/kuesionersiswa/list',$data);
	}

}
