<?php
defined('BASEPATH') OR exit('No direct scripts access allowed');

class Kuesioner extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$logged_in = $this->session->userdata('logged_in');
		$level = $this->session->userdata('level');
		if(empty($logged_in))
		{
			redirect('auth/login');
		}
		if($level != 'admin')
		{
			redirect('auth/login');
		}
	}

	public function index()
	{
		$data['kuesioner'] = $this->db->get('kuesioner')->result();
		$this->load->view('admin/kuesioner/list',$data);
	}

	public function create()
	{
		$this->load->view('admin/kuesioner/create');
	}

	//Menyimpan data baru surat ke database
	public function store()
	{
		$data['pertanyaan'] = $this->input->post('pertanyaan',true);
		$this->db->insert('kuesioner',$data);
		$this->session->set_flashdata('berhasil','Pertanyaan berhasil ditambahkan');
			redirect('admin/kuesioner/create');
		}

		public function editkuesioner($id_kuesioner)
		{
			$data['kuesioner'] = $this->db->where('id_kuesioner',$id_kuesioner)->get('kuesioner')->row();
			$this->load->view('admin/kuesioner/edit',$data);
		}

		public function updatekuesioner($id_kuesioner)
		{
			$data['pertanyaan'] = $this->input->post('pertanyaan',true);
			$nama = $this->input->post('pertanyaan',true);
			$this->db->where('id_kuesioner',$id_kuesioner)->update('kuesioner',$data);
			$this->session->set_flashdata('berhasil','Pertanyaan berhasil di Update');
			redirect('admin/kuesioner/editkuesioner/'.$id_kuesioner);
		}

		public function destroykuesioner($id_kuesioner)
		{
			$this->db->where('id_kuesioner',$id_kuesioner)->delete('kuesioner');
			redirect('admin/kuesioner');
		}

		public function cetakkuesioner()
		{
			$data['kuesioner'] = $this->db->get('kuesioner')->result();

			$html = $this->load->view('admin/kuesioner/cetak',$data, true);

			require(APPPATH."/third_party/html2pdf_4_03/html2pdf.class.php");
			try {
				$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array('20', '5', '20', '5'));
				$html2pdf->WriteHTML($html);
				$html2pdf->Output('laporan_kuesioner_'.date('Ymd').'.pdf');
			} catch (HTML2PDF_exception $e) {
	            // echo $e;
				$this->session->set_flashdata('berhasil', 'Maaf, kami mengalami kendala teknis.');
				redirect('admin/kuesioner');
			}
		}
	}
