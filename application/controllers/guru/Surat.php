<?php
defined('BASEPATH') OR exit('No direct scripts access allowed');

class Surat extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$logged_in = $this->session->userdata('logged_in');
		$level = $this->session->userdata('level');
		if(empty($logged_in))
		{
			redirect('home');
		}
		if($level != 'guru')
		{
			redirect('home');
		}

	}

	public function index()
	{
		$nik = $this->session->userdata('nik');
		$data['profil'] = $this->db->where('nip',$nik)->get('guru')->row();
    $data['surat'] = $this->db->get('surat')->result();
    $this->load->view('guru/surat/index',$data);
    }

    public function surat()
  {
		$nik = $this->session->userdata('nik');
		$data['profil'] = $this->db->where('nip',$nik)->get('guru')->row();
    $data['surat'] = $this->db->get('surat')->result();
    $this->load->view('guru/surat/index',$data);
  }

  public function editsurat($id_surat)
  {
		$nik = $this->session->userdata('nik');
		$data['profil'] = $this->db->where('nip',$nik)->get('guru')->row();
    $data['surat'] = $this->db->where('id_surat',$id_surat)->get('surat')->row();
    $this->load->view('guru/surat/edit',$data);
  }

  public function updatesurat($id_surat)
  {
    $data['nomor_surat'] = $this->input->post('nomor_surat',true);
    $data['perihal'] = $this->input->post('perihal',true);
    $data['tujuan_surat'] = $this->input->post('tujuan_surat',true);
    $data['tanggal_surat'] = $this->input->post('tanggal_surat',true);
    $data['pengolah'] = $this->input->post('pengolah',true);
    $data['tanggal_terima'] = $this->input->post('tanggal_terima',true);
    $data['keterangan'] = $this->input->post('keterangan',true);

    $nama = $this->input->post('nomor_surat',true);

    $this->db->where('id_surat',$id_surat)->update('surat',$data);

    $this->session->set_flashdata('berhasil','Surat dengan nomor surat <b>'.$nama.'</b> berhasil di Update');

    redirect('guru/editsurat/'.$id_surat);
  }

  public function destroysurat($id_surat)
  {
    $this->db->where('id_surat',$id_surat)->delete('surat');
    redirect('guru/surat');
  }

  public function cetaksurat()
  {
    $data['surat'] = $this->db->get('surat')->result();
    $html = $this->load->view('guru/surat/cetak',$data, true);

    require(APPPATH."/third_party/html2pdf_4_03/html2pdf.class.php");
    try {
      $html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array('20', '5', '20', '5'));
      $html2pdf->WriteHTML($html);
      $html2pdf->Output('laporan_surat_'.date('Ymd').'.pdf');
    } catch (HTML2PDF_exception $e) {
      $this->session->set_flashdata('berhasil', 'Maaf, kami mengalami kendala teknis.');
      redirect('guru/surat');
    }
  }
}
