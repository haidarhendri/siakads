<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view('user/layouts/header'); ?>
    </head>



    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <?php $this->load->view('user/layouts/top_menu');?>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->

            <?php $this->load->view('user/layouts/sidebar');?>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-default">
                        <?php $berhasil = $this->session->flashdata('berhasil');
                            if(!empty($berhasil)){ ?>

                          <div class="alert alert-warning alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <?= $this->session->flashdata('berhasil'); ?>
                          </div>

                        <?php } ?>
                          <div class="panel-heading">
                              <h3 class="panel-title">Materi Listing</h3>
                          </div>

                          <div class="panel-body">
                              <table class="table table-striped">
                                  <tr>
                        						<!-- <th>ID</th> -->
                        						<th>Mata Pelajaran</th>
                        						<th>Kelas</th>
                        						<th>Nama Materi</th>
                        						<th>Status</th>
                                  </tr>
                                  <?php foreach($materi as $m){ ?>
                                  <tr>
                        						<!-- <td><?php echo $m['id_materi']; ?></td> -->
                        						<td><?php echo $m['nama_mapel']; ?></td>
                        						<td><?php echo $m['nama_ruangan']; ?></td>
                        						<td><?php echo $m['materi']; ?></td>
                        						<td><?php echo $m['flag']; ?></td>
                                </tr>
                                <?php } ?>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> <!-- container -->
                </div> <!-- content -->

          <footer class="footer text-right">
          2018 © DevTeamUNS.
          </footer>
        </div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->

<!-- /Right-bar -->

    </div>
<!-- END wrapper -->
    <?php $this->load->view('user/layouts/footer'); ?>
  </body>
</html>
