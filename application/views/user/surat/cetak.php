<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Rekap Surat</title>
    <style type="text/css">
        h1 {
            text-align:center;
            font-size:18px;
        }

        table {
            font-size:10px;
            border-collapse: collapse;
        }
        .zebra {
            background-color:#CCCCCC;
        }
        th, td {
            padding: 4px 2px;
        }
        th, tfoot tr td {
            background-color: #999999;
        }
    </style>
</head>

<body>
    <h1>Rekap Data Surat</h1>

    <table width="600" border="0">
        <thead>
            <tr>
              <th style="padding-right: 20px;">No</th>
              <th style="padding-right: 20px;">Nomor Surat</th>
              <th style="padding-right: 20px;">Perihal</th>
              <th style="padding-right: 20px;">Tujuan Surat</th>
              <th style="padding-right: 20px;">Tanggal Surat</th>
              <th style="padding-right: 20px;">Pengolah</th>
              <th style="padding-right: 20px;">Tanggal Terima</th>
              <th style="padding-right: 20px;">Keterangan</th>
            </tr>
        </thead>
        <tbody>
          <?php $nomor = 0; ?>
            <?php foreach ($surat as $su): ?>
                <?= ($nomor & 1) ? '<tr class="zebra">' : '<tr>'; ?>
                <td style="padding-right: 20px;" width="30"><?= $nomor;?> </td>
                <?php $nomor++;?>
                <td style="padding-right: 20px;"><?= $su->nomor_surat; ?>
                </td>
                <td style="padding-right: 20px;"><?= $su->perihal; ?></td>
                <td style="padding-right: 20px;"><?= $su->tujuan_surat; ?></td>
                <td style="padding-right: 20px;"><?= $su->tanggal_surat; ?></td>
                <td style="padding-right: 20px;"><?= $su->pengolah; ?></td>
                <td style="padding-right: 20px;"><?= $su->tanggal_terima; ?></td>
                <td style="padding-right: 20px;"><?= $su->keterangan; ?></td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>

</body>
</html>
