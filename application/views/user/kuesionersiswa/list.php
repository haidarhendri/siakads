<!DOCTYPE html>
<html>
<head>
    <?php $this->load->view('user/layouts/header'); ?>
</head>

<body class="fixed-left">
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <?php $this->load->view('user/layouts/top_menu');?>
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
        <?php $this->load->view('user/layouts/sidebar');?>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                    <!-- Page-Title -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">

                                <?php
                                $berhasil = $this->session->flashdata('berhasil');

                                if(!empty($berhasil))
                                    { ?>

                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= $this->session->flashdata('berhasil'); ?>
                                </div>
                                <?php }  ?>

                                <div class="panel-heading"><h3 class="panel-title">Data Pertanyaan <br>[NIK anda adalah = <?= $nik ?>]</h3></div>
                                <div class="panel-body">
                                    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSc2j9u4WoRp5UWRnYUeSl-Jr9WwaLEGGt7EyS03BU0GDSp8Iw/viewform?embedded=true" width="100%" height="1600" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
                                    </div> <!-- panel-body -->
                                </div> <!-- panel -->
                            </div> <!-- col -->
                        </div> <!-- End row -->
                        <!-- end row -->
                    </div> <!-- container -->
                </div> <!-- content -->
                <footer class="footer text-right">
                    2018 © DevIf16Uns.
                </footer>
            </div>

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <!-- Right Sidebar -->
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->

<?php $this->load->view('user/layouts/footer'); ?>

</body>
</html>
