<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Materi Add</h3>
            </div>
            <?php echo form_open('materi/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="id_mapel" class="control-label"><span class="text-danger">*</span>Id Mapel</label>
						<div class="form-group">
							<input type="text" name="id_mapel" value="<?php echo $this->input->post('id_mapel'); ?>" class="form-control" id="id_mapel" />
							<span class="text-danger"><?php echo form_error('id_mapel');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_kelas" class="control-label"><span class="text-danger">*</span>Id Kelas</label>
						<div class="form-group">
							<input type="text" name="id_kelas" value="<?php echo $this->input->post('id_kelas'); ?>" class="form-control" id="id_kelas" />
							<span class="text-danger"><?php echo form_error('id_kelas');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="materi" class="control-label"><span class="text-danger">*</span>Materi</label>
						<div class="form-group">
							<input type="text" name="materi" value="<?php echo $this->input->post('materi'); ?>" class="form-control" id="materi" />
							<span class="text-danger"><?php echo form_error('materi');?></span>
						</div>
					</div>
					<div class="col-md-6">
						<label for="flag" class="control-label"><span class="text-danger">*</span>Flag</label>
						<div class="form-group">
							<input type="text" name="flag" value="<?php echo $this->input->post('flag'); ?>" class="form-control" id="flag" />
							<span class="text-danger"><?php echo form_error('flag');?></span>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>