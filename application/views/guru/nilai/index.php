<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('guru/layouts/header');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php $this->load->view('guru/layouts/top_menu');?>

    <!-- Left side column. contains the logo and sidebar -->

    <?php $this->load->view('guru/layouts/sidebar');?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-page">
      <!-- Start content -->
      <div class="content">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="panel panel-default">
                  <?php
                      $berhasil = $this->session->flashdata('berhasil');

                      if(!empty($berhasil))
                      { ?>

                      <div class="alert alert-warning alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <?= $this->session->flashdata('berhasil'); ?>
                      </div>

                  <?php } ?>


      <!-- Content Header (Page header) -->
      <!-- <section class="content-header">
        <h1>PROFIL
        </h1>
        <ol class="breadcrumb">

          <li><a href="#"><i class="fa fa-dashboard"></i> MTs.Fisabilillah</a></li>
          <li class="active">Profil</li>
        </ol>
      </section> -->


      <!-- Main content -->
      <!-- <section class="content"> -->

        <!-- /.content -->

        <div class="panel-heading">
            <h3 class="panel-title">Nilai Siswa</h3>
        </div>
        <div class="panel-body">
          <a href="<?=site_url('guru/nilai/cetak2');?>" target="_blank" class="btn btn-success" style="width: 20%; margin-bottom: 20px" target="_blank">Cetak</a>

        <table class="table table-striped">
          <!-- <thead> -->
            <tr>
              <th>Nama Siswa</th>
              <th>Kelas</th>
              <th>Mata Pelajaran</th>
              <th>Semester</th>
              <th>Tahun Ajaran</th>
              <th>Tugas</th>
              <th>UTS</th>
              <th>UAS</th>
              <th>Rata-Rata</th>
              <th>Action</th>
            </tr>
          <!-- </thead>
          <tbody> -->
            <?php foreach($nilai as $nilai) : ?>
              <tr>
                <td><?= $nilai->nama;?></td>
                <td><?= $nilai->nama_ruangan;?></td>
                <td><?= $nilai->nama_mapel;?></td>
                <td><?= $nilai->semester;?></td>
                <td><?= $nilai->thn_ajaran;?></td>
                <td><?= $nilai->tugas;?></td>
                <td><?= $nilai->uts;?></td>
                <td><?= $nilai->uas;?></td>
                <td><?= $nilai->rata;?></td>
                <td>
                    <a href="<?php echo site_url('guru/nilai/remove/'.$nilai->id); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                </td>
              </tr>
            <?php endforeach;?>
          <!-- </tbody> -->
        </table>
        <!-- /.content -->
      </div>
    </div>
    </div>
  </div>
</div>
</div> <!-- container -->
</div> <!-- content -->

      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.
      </footer>

  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('guru/layouts/footer');?>
</body>
</html>
