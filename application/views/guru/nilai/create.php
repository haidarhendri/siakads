<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('guru/layouts/header');?>
</head>
<body class="fixed-left">
  <div id="wrapper">

    <?php $this->load->view('guru/layouts/top_menu');?>

    <!-- Left side column. contains the logo and sidebar -->

    <?php $this->load->view('guru/layouts/sidebar');?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                  <div class="col-md-12">
                      <div class="panel panel-default">
                        <?php
                            $berhasil = $this->session->flashdata('berhasil');

                            if(!empty($berhasil))
                            { ?>

                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?= $this->session->flashdata('berhasil'); ?>
                            </div>

                        <?php } ?>

                          <div class="panel-heading">
                              <h3 class="panel-title">Input Nilai</h3>
                          </div>
                          <?php echo form_open('guru/nilai/store'); ?>
                          <div class="panel-body">
                            <div class="form-horizontal">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Siswa</label>
                                <div class="col-sm-10">
                                  <select name="nik_siswa" class="form-control">
                                    <option disable selected="">Pilih Siswa</option>
                                    <?php foreach($siswa as $siswa) : ?>
                                      <option value="<?=$siswa->nik;?>"><?=$siswa->nama;?></option>
                                    <?php endforeach;?>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                                <div class="col-sm-10">
                                  <select name="id_kelas" class="form-control">
                                    <option disable selected="">Pilih Kelas</option>
                                    <?php foreach($kelas as $ks) : ?>
                                      <option value="<?=$ks->id;?>"><?=$ks->nama_ruangan;?></option>
                                    <?php endforeach;?>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Mata Pelajaran</label>
                                <div class="col-sm-10">
                                  <select name="mapel" class="form-control">
                                    <option disabled selected="">Pilih Mapel</option>
                                    <?php foreach($mapel as $mapel) : ?>
                                      <option value="<?=$mapel->kode_mapel;?>"><?=$mapel->nama_mapel;?></option>
                                    <?php endforeach;?>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Semester</label>
                                <div class="col-sm-10">
                                  <select name="semester" class="form-control">
                                    <option disabled selected="">Pilih Semester</option>
                                    <option value="genap">Genap</option>
                                    <option value="ganjil">Ganjil</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Tahun Ajaran</label>
                                <div class="col-sm-10">
                                  <select name="thn_ajaran" class="form-control required">
                                    <option disabled selected>Tahun Ajaran</option>
                                    <?php for($i=2009;$i<=2100;$i++){$a = $i+1; echo "<option>$i-$a</option>";}?>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Tugas</label>
                                <div class="col-sm-10">
                                  <input type="number" name="tugas" class="form-control">
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">UTS</label>
                                <div class="col-sm-10">
                                  <input type="number" name="uts" class="form-control">
                                </div>
                              </div>

                              <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">UAS</label>
                                <div class="col-sm-10">
                                  <input type="number" name="uas" class="form-control">
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-info waves-effect waves-light">
                                  <i class="fa fa-check"></i> Input Nilai
                                </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php echo form_close(); ?>
                      </div>
                  </div>
              </div>
          <!-- end row -->

      </div> <!-- container -->

  </div> <!-- content -->
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
      </div>
      <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
      reserved.
    </footer>

  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('guru/layouts/footer');?>
</body>
</html>
