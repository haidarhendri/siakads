<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('guru/layouts/header');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php $this->load->view('guru/layouts/top_menu');?>

    <!-- Left side column. contains the logo and sidebar -->

    <?php $this->load->view('guru/layouts/sidebar');?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-page">
      <!-- Start content -->
      <div class="content">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="panel panel-default">
                  <?php
                      $berhasil = $this->session->flashdata('berhasil');

                      if(!empty($berhasil))
                      { ?>

                      <div class="alert alert-warning alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <?= $this->session->flashdata('berhasil'); ?>
                      </div>

                  <?php } ?>
      <!-- Content Header (Page header) -->
      <div class="panel-heading">
          <h3 class="panel-title">Jadwal Mengajar (<?=$this->session->userdata('nama');?>)</h3>
      </div>
      <div class="panel-body">
        <a href="<?=site_url('guru/nilai/cetak2');?>" target="_blank" class="btn btn-success" style="width: 20%; margin-bottom: 20px" target="_blank">Cetak</a>

      <table class="table table-striped">

        <!-- /.content -->
        <!-- <table class="data table table-bordered" id="datatable-editable">
          <thead> -->
            <tr>
              <th>No</th>
                <th>Hari</th>
                <th>Kelas</th>
                <th>Mata Pelajaran</th>
                <th>Jam</th>
            </tr>
          <!-- </thead>
          <tbody> -->
          <?php $no=0;?>
          <?php foreach($senin as $senin) : ?>
              <tr>
                <td><?= ++$no;?></td>
                <td><?= $senin->hari;?></td>
                <td><?= $senin->nama_ruangan;?></td>
                <td><?= $senin->nama_mapel;?></td>
                <td><?= $senin->jam_awal;?>:<?= $senin->menit_awal;?>-<?= $senin->jam_akhir;?>:<?= $senin->menit_akhir;?></td>
              </tr>
            <?php endforeach;?>

            <?php foreach($selasa as $senin) : ?>
              <tr>
                <td><?= ++$no;?></td>
                <td><?= $senin->hari;?></td>
                <td><?= $senin->nama_ruangan;?></td>
                <td><?= $senin->nama_mapel;?></td>
                <td><?= $senin->jam_awal;?>:<?= $senin->menit_awal;?>-<?= $senin->jam_akhir;?>:<?= $senin->menit_akhir;?></td>
              </tr>
            <?php endforeach;?>

            <?php foreach($rabu as $senin) : ?>
              <tr>
                <td><?= ++$no;?></td>
                <td><?= $senin->hari;?></td>
                <td><?= $senin->nama_ruangan;?></td>
                <td><?= $senin->nama_mapel;?></td>
                <td><?= $senin->jam_awal;?>:<?= $senin->menit_awal;?>-<?= $senin->jam_akhir;?>:<?= $senin->menit_akhir;?></td>
              </tr>
            <?php endforeach;?>

            <?php foreach($kamis as $senin) : ?>
              <tr>
                <td><?= ++$no;?></td>
                <td><?= $senin->hari;?></td>
                <td><?= $senin->nama_ruangan;?></td>
                <td><?= $senin->nama_mapel;?></td>
                <td><?= $senin->jam_awal;?>:<?= $senin->menit_awal;?>-<?= $senin->jam_akhir;?>:<?= $senin->menit_akhir;?></td>
              </tr>
            <?php endforeach;?>

            <?php foreach($jumat as $senin) : ?>
              <tr>
                <td><?= ++$no;?></td>
                <td><?= $senin->hari;?></td>
                <td><?= $senin->nama_ruangan;?></td>
                <td><?= $senin->nama_mapel;?></td>
                <td><?= $senin->jam_awal;?>:<?= $senin->menit_awal;?>-<?= $senin->jam_akhir;?>:<?= $senin->menit_akhir;?></td>
              </tr>
            <?php endforeach;?>

            <?php foreach($sabtu as $senin) : ?>
              <tr>
                <td><?= ++$no;?></td>
                <td><?= $senin->hari;?></td>
                <td><?= $senin->nama_ruangan;?></td>
                <td><?= $senin->nama_mapel;?></td>
                <td><?= $senin->jam_awal;?>:<?= $senin->menit_awal;?>-<?= $senin->jam_akhir;?>:<?= $senin->menit_akhir;?></td>
              </tr>
            <?php endforeach;?>
          <!-- </tbody> -->
        </table>
        <!-- /.content -->
      <!-- </section> -->
    </div>
  </div>
  </div>
</div>
</div>
</div> <!-- container -->
</div> <!-- content -->
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
      </div>
      <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
      reserved.
    </footer>

  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('guru/layouts/footer');?>
</body>
</html>
