<!DOCTYPE html>
<html>
<head>
    <?php $this->load->view('guru/layouts/header'); ?>
</head>



<body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <?php $this->load->view('guru/layouts/top_menu');?>
        <!-- Top Bar End -->


        <!-- ========== Left Sidebar Start ========== -->

        <?php $this->load->view('guru/layouts/sidebar');?>
        <!-- Left Sidebar End -->



        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">

                    <!-- Page-Title -->
                    <div class="row">
                      <div class="col-md-12">
                        	<div class="panel panel-default">
                            <?php
                                $berhasil = $this->session->flashdata('berhasil');

                                if(!empty($berhasil))
                                { ?>

                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= $this->session->flashdata('berhasil'); ?>
                                </div>

                            <?php } ?>

                              <div class="panel-heading">
                                	<h3 class="panel-title">Input Materi Mata Pelajaran</h3>
                              </div>
                              <?php echo form_open('guru/materi/edit/'.$materi['id_materi']); ?>
                              <div class="panel-body">
                                <div class="form-horizontal">
                                  <div class="form-group">
                                    <label for="id_mapel" class="col-md-3 control-label"><span class="text-danger">*</span>Mata Pelajaran</label>
                                    <div class="col-md-8">
                                      <!-- <input type="text" name="id_mapel" value="<?php echo ($this->input->post('id_mapel') ? $this->input->post('id_mapel') : $materi['id_mapel']); ?>" class="form-control" id="id_mapel" /> -->
                                      <select id="id_mapel" name="id_mapel" class="form-control" value="<?php echo ($this->input->post('id_mapel') ? $this->input->post('id_mapel') : $materi['id_mapel']); ?>">
                                      <?php

                                      foreach($mapel as $row)
                                      {
                                        echo '<option value="'.$row->id.'">'.$row->nama_mapel.'</option>';
                                      }
                                      ?>
                                      </select>
                                      <span class="text-danger"><?php echo form_error('id_mapel');?></span>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="id_kelas" class="col-md-3 control-label"><span class="text-danger">*</span>Kelas</label>
                                    <div class="col-md-8">
                                      <!-- <input type="text" name="id_kelas" value="<?php echo ($this->input->post('id_kelas') ? $this->input->post('id_kelas') : $materi['id_kelas']); ?>" class="form-control" id="id_kelas" /> -->
                                      <select id="id_kelas" name="id_kelas" class="form-control" value="<?php echo ($this->input->post('id_kelas') ? $this->input->post('id_kelas') : $materi['id_kelas']); ?>">
                                      <?php

                                      foreach($kelas as $row)
                                      {
                                        echo '<option value="'.$row->id.'">'.$row->nama_ruangan.'</option>';
                                      }
                                      ?>
                                      </select>
                                      <span class="text-danger"><?php echo form_error('id_kelas');?></span>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="materi" class="col-md-3 control-label"><span class="text-danger">*</span>Nama Materi</label>
                                    <div class="col-md-8">
                                      <input type="text" name="materi" value="<?php echo ($this->input->post('materi') ? $this->input->post('materi') : $materi['materi']); ?>" class="form-control" id="materi" />
                                      <span class="text-danger"><?php echo form_error('materi');?></span>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="flag" class="col-md-3 control-label"><span class="text-danger">*</span>Status</label>
                                    <div class="col-md-8">
                                      <input type="text" name="flag" value="<?php echo ($this->input->post('flag') ? $this->input->post('flag') : $materi['flag']); ?>" class="form-control" id="flag" />
                                      <span class="text-danger"><?php echo form_error('flag');?></span>
                                    </div>
                                  </div>
                                  <div class="form-group m-b-0">
                                    <div class="col-sm-offset-3 col-sm-9">
                                      <button type="submit" class="btn btn-info waves-effect waves-light">
                                        <i class="fa fa-check"></i> Tambah
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <?php echo form_close(); ?>
                        	</div>
                      </div>
                  </div>
              <!-- end row -->

          </div> <!-- container -->

      </div> <!-- content -->

      <footer class="footer text-right">
        2015 © Moltran.
    </footer>

</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->

<!-- /Right-bar -->

</div>
<!-- END wrapper -->



<?php $this->load->view('guru/layouts/footer'); ?>

</body>
</html>
