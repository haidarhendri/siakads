<!DOCTYPE html>
<html>
<head>
    <?php $this->load->view('admin/layouts/header'); ?>
</head>



<body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <?php $this->load->view('admin/layouts/top_menu');?>
        <!-- Top Bar End -->


        <!-- ========== Left Sidebar Start ========== -->

        <?php $this->load->view('admin/layouts/sidebar');?>
        <!-- Left Sidebar End -->



        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">

                    <!-- Page-Title -->
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="panel panel-default">

                                <?php
                                $berhasil = $this->session->flashdata('berhasil');

                                if(!empty($berhasil))
                                    { ?>

                                <div class="alert alert-warning alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= $this->session->flashdata('berhasil'); ?>
                                </div>

                                <?php }

                                ?>


                                <div class="panel-heading"><h3 class="panel-title">Edit Data Surat</h3></div>
                                <div class="panel-body">
                                    <form action="<?php echo site_url('admin/lihatdata/updatesurat/'.$surat->id_surat);?>" method="POST" class="form-horizontal" role="form">
                                      <div class="form-group">
                                          <label class="col-md-2 control-label">Nomor Surat</label>
                                          <div class="col-md-10">
                                              <input type="text" name="nomor_surat" class="form-control" value="<?= $surat->nomor_surat;?>">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label class="col-md-2 control-label">Perihal</label>
                                          <div class="col-md-10">
                                              <input type="text" name="perihal" class="form-control" value="<?= $surat->perihal;?>">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label class="col-md-2 control-label">Tujuan Surat</label>
                                          <div class="col-md-10">
                                              <select name="tujuan_surat" class="form-control">
                                                  <option disabled selected>Pilih Tujuan</option>
                                                      <option value="guru">Guru</option>
                                                      <option value="siswa">Siswa</option>
                                              </select>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label class="col-md-2 control-label" for="example-email">Tanggal Surat</label>
                                          <div class="col-md-10">
                                              <input type="text" id="ddtt" name="tanggal_surat" class="form-control" placeholder="Pilih Tanggal">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label class="col-md-2 control-label">Pengolah Surat</label>
                                          <div class="col-md-10">
                                              <input type="text" name="pengolah" class="form-control" value="<?= $surat->pengolah;?>">
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label class="col-md-2 control-label" for="example-email">Tanggal Terima</label>
                                          <div class="col-md-10">
                                              <!-- <input type="text" name="tanggal_terima" class="form-control" placeholder="Isi Tanggal Terima Surat"> -->
                                              <input type="text" class="form-control" id="date" name="tanggal_terima" value="<?= $surat->tanggal_terima;?>" readonly>
                                              <!-- <script type="text/javascript">
                                                document.getElementById('date').value = Date();
                                              </script> -->
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label class="col-md-2 control-label">Keterangan</label>
                                          <div class="col-md-10">
                                              <input type="text" name="keterangan" class="form-control" value="<?= $surat->keterangan;?>">
                                          </div>
                                      </div>

                                      <!-- <div class="form-group">
                                          <label class="col-md-2 control-label">Upload Lampiran</label>
                                          <div class="col-md-10">
                                              <input type="file" name="userfile" />
                                          </div>
                                      </div> -->
                                      <div class="form-group m-b-0">
                                          <div class="col-sm-offset-2 col-sm-9">
                                              <button type="submit" class="btn btn-info waves-effect waves-light">Tambah</button>
                                          </div>
                                      </div>
                                    </form>
                                </div> <!-- panel-body -->
                            </div> <!-- panel -->
                        </div> <!-- col -->
                    </div> <!-- End row -->
                    <!-- end row -->

                </div> <!-- container -->

            </div> <!-- content -->

            <footer class="footer text-right">
                2015 © Moltran.
            </footer>

        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->


        <!-- Right Sidebar -->

        <!-- /Right-bar -->

    </div>
    <!-- END wrapper -->



    <?php $this->load->view('admin/layouts/footer'); ?>

</body>
</html>
