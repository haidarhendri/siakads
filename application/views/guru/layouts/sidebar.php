<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div class="user-details">
            <div class="pull-left">
              <?php $poto = $profil->photo;?>
                <img src="<?php echo base_url('assets/upload/guru/'.$poto);?>" alt="" class="thumb-md img-circle">
            </div>
            <div class="user-info">
                <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?= $this->session->userdata('nama'); ?></a>
                </div>
                <p class="text-muted m-0">Guru</p>
            </div>
        </div>
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="<?= site_url('guru/dashboard');?>" class="waves-effect active"><i class="md md-home"></i><span> Dashboard </span></a>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect"><i class="md md-mail"></i><span> Nilai </span><span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                      <li><a href="<?= site_url('guru/nilai');?>">Input Nilai</a></li>
                      <li><a href="<?=site_url('guru/nilai/lihat_nilai');?>">Lihat Nilai</a></li>
                      <li><a href="<?=site_url('guru/nilai/create_ekskul');?>">Input Nilai Ekstrakurikuler</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="<?= site_url('guru/jadwal');?>" class="waves-effect"><i class="md md-palette"></i> <span> Jadwal Pelajaran </span> <span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?= site_url('guru/jadwal');?>">Jadwal</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect"><i class="md md-invert-colors-on"></i><span> RPS </span><span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                      <li><a href="<?= site_url('guru/materi/add');?>">Input RPS</a></li>
                      <li><a href="<?=site_url('guru/materi/index');?>">Lihat RPS</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="#" class="waves-effect"><i class="fa fa-paper-plane"></i><span> Kuesioner </span><span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo site_url('guru/kuesioner');?>"> Kuesioner</a></li>
                        <li><a href="<?php echo site_url('guru/kuesioner/create');?>">Input Kuesioner</a></li>
                        <li><a href="<?php echo site_url('guru/kuesionersiswa');?>">Kuesioner Siswa</a></li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
