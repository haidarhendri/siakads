<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('guru/layouts/header');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php $this->load->view('guru/layouts/top_menu');?>

    <!-- Left side column. contains the logo and sidebar -->

    <?php $this->load->view('guru/layouts/sidebar');?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-page">
      <!-- Start content -->
      <div class="content">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="panel panel-default">
                  <?php
                      $berhasil = $this->session->flashdata('berhasil');

                      if(!empty($berhasil))
                      { ?>

                      <div class="alert alert-warning alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <?= $this->session->flashdata('berhasil'); ?>
                      </div>

                  <?php } ?>

        <div class="panel-heading">
            <h3 class="panel-title">Profil Guru</h3>
        </div>
        <div class="panel-body">
          <div class="box box-primary">
            <div class="box-body box-profile">
              <?php $poto = $profil->photo;?>

              <?php if (empty($poto)) : ?>
                <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('assets/upload/guru/default.png');?>" alt="Guru profile picture">
              <?php else : ?>
                <img class="profile-user-img img-responsive img-circle center"
                style="max-width: 25%; margin-left: auto; margin-right: auto;"
                src="<?php echo base_url('assets/upload/guru/'.$poto);?>" alt="Guru profile picture">
              <?php endif;?>

              <h3 class="profile-username text-center"><?php echo $profil->nama;?></h3>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>NIP</b> <a class="pull-right"><?= $profil->NIP;?></a>
                </li>
                <li class="list-group-item">
                  <b>Jabatan</b> <a class="pull-right"><?= $profil->jabatan;?></a>
                </li>
                <li class="list-group-item">
                  <b>No. Handphone</b> <a class="pull-right"><?= $profil->nope;?></a>
                </li>
                <li class="list-group-item">
                  <b>TTL</b> <a class="pull-right"><?= $profil->ttl;?></a>
                </li>
                <li class="list-group-item">
                  <b>Jenis Kelamin</b> <a class="pull-right"><?php echo ($profil->jk == 'L') ? 'Laki-Laki' : 'Perempuan';?></a>
                </li>
                <li class="list-group-item">
                  <b>Alamat</b> <a class="pull-right"><?= $profil->alamat;?></a>
                </li>
              </ul>

            </div>
            <!-- /.box-body -->
          </div>
        <!-- /.content -->
      </div>
    </div>
    </div>
  </div>
</div>
</div> <!-- container -->
</div> <!-- content -->

      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.
      </footer>

  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->load->view('guru/layouts/footer');?>
</body>
</html>
