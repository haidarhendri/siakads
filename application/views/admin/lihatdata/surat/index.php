<!DOCTYPE html>
<html>
<head>
    <?php $this->load->view('admin/layouts/header'); ?>
</head>



<body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <?php $this->load->view('admin/layouts/top_menu');?>
        <!-- Top Bar End -->


        <!-- ========== Left Sidebar Start ========== -->

        <?php $this->load->view('admin/layouts/sidebar');?>
        <!-- Left Sidebar End -->



        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">

                    <!-- Page-Title -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">

                                <?php
                                $berhasil = $this->session->flashdata('berhasil');

                                if(!empty($berhasil))
                                    { ?>

                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= $this->session->flashdata('berhasil'); ?>
                                </div>

                                <?php }

                                ?>


                                <div class="panel-heading"><h3 class="panel-title">Data Surat</h3></div>
                                <div class="panel-body">
                                    <!-- Isi COntent ====================== -->

                                    <table class="datatables table table-bordered table-striped" id="datatable-editable">
                                        <a href="<?=site_url('admin/lihatdata/cetaksurat');?>" target="_blank" class="btn btn-success" style="width: 20%; margin-bottom: 20px;">CETAK</a>
                                        <table class="data table table-striped">
                                            <thead>
                                                <tr>
                                                  <th>No</th>
                                                  <th>Nomor Surat</th>
                                                  <th>Perihal</th>
                                                  <th>Tujuan Surat</th>
                                                  <th>Tanggal Surat</th>
                                                  <th>Pengolah</th>
                                                  <th>Tanggal Terima</th>
                                                  <th>Keterangan</th>
                                                  <th>File</th>
                                                  <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $nomor = 1; ?>
                                                <?php foreach($surat as $sw) : ?>
                                                    <tr class="gradeX">
                                                        <td><?= $nomor;?> </td>
                                                        <?php $nomor++;?>
                                                        <td><?= $sw->nomor_surat; ?></td>
                                                        <td><?= $sw->perihal; ?></td>
                                                        <td><?= $sw->tujuan_surat; ?></td>
                                                        <td><?= $sw->tanggal_surat; ?></td>
                                                        <td><?= $sw->pengolah; ?></td>
                                                        <td><?= $sw->tanggal_terima; ?></td>
                                                        <td><?= $sw->keterangan; ?></td>
                                                        <td><button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal">BUKA</button>
                                                          <div id="myModal" class="modal fade" role="dialog">
                                                          <div class="modal-dialog modal-lg">
                                                              <!-- Modal content-->
                                                              <div class="modal-content">
                                                                  <div class="modal-header">
                                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                      <h4 class="modal-title"><?= $sw->nomor_surat; ?></h4>
                                                                  </div>
                                                                  <div class="modal-body">
                                                                      <embed src="http://localhost/siakads/assets/upload/surat/<?= $sw->upload; ?>"
                                                                             frameborder="0" width="100%" height="450px">
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      </td>
                                                        <td class="actions">
                                                            <a href="<?=site_url('admin/lihatdata/editsurat/'.$sw->id_surat);?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>
                                                            <a href="<?=site_url('admin/lihatdata/destroysurat/'.$sw->id_surat);?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Hapus</a>
                                                        </td>

                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>

                                    </div> <!-- panel-body -->
                                </div> <!-- panel -->
                            </div> <!-- col -->
                        </div> <!-- End row -->
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2015 © Moltran.
                </footer>

            </div>

            <!-- Modal Edit -->
            <div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none" id="urlhapus" url="<?php echo site_url('admin/lihatdata/destroysurat/');?>"></div>

    <div style="display: none" id="urledit" url="<?php echo site_url('admin/lihatdata/editsurat/');?>"></div>

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->

    <!-- /Right-bar -->

</div>
<!-- END wrapper -->



<?php $this->load->view('admin/layouts/footer'); ?>

<script>
    $(document).ready(function(){
        $('.hapus').click(function(e) {
            var id = $(this).attr('surat');
            e.preventDefault();
            swal({
              title: "Are you sure?",
              text: "You will not be able to recover this imaginary file!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false
          },
          function(){
            var url = $('#urlhapus').attr('url');

            window.location.href = url + '/' + id;
        });
        });
    });
</script>

</body>
</html>
